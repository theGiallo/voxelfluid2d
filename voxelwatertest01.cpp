/**
 * Introduction to SDL 2.0
 * by Gianluca Alloisio a.k.a. theGiallo
 * --------------------------------------------------------------------------------
 *
 * = Lecture 5 =
 * 
 * 
 * Prerequisites:
 *  1- function calling
 *  2- carthesian coordinate system
 *  3- draw lines, points and rectangles
 *  4- fill rectangles
 *  5- clear screen
 *  6- change color
 *  7- concept of vectorial velocity
 *  8- game loop
 * 
 * Objectives:
 *  1- load ttf fonts
 *  2- render text with custom settings
 * 
 * Tasks:
 * 
 *  0- look at the present code that shows you most of SDL_ttf and that renders the "Hello world!"s
 *  1- render the infos printed each frame at the top left corner of the screen
 *  2- make a function that correctly renders a given text with breaklines at a given position with a given font
 *  3- use the getTimeInSec to calc the time duration of each different text render call with the same text and render them
 *  4- make a function that creates (only once) a buffer with the ASCII characters textures and uses them to render a given text (use TTF_RenderGlyph_* to create the buffer)
 *  5- make a function like the above one that uses a texture atlas instead of a buffer
 * 
 **/

#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#include <SDL_ttf.h>
#include <iostream>
#include <cmath>
#include <utility> // for std::move
#include <random>
#include <chrono>

#ifndef MIN
    #define MIN(a,b) ((a)<(b)?(a):(b))
#endif
#ifndef MAX
    #define MAX(a,b) ((a)<(b)?(a):(b))
#endif
#ifndef SIGN
    #define SIGN(a) (((a) > 0) - ((a) < 0))
#endif

const double MAX_FPS = 120.0;
const double MIN_FPS = 16.0;
const double EXPECTED_FPS = 60.0;
const double MAX_UPS = 300.0;//300.0;
const double FIXED_TIMESTEP = 1.0 / MAX_UPS;
const double FIXED_TIMESTEP_2 = FIXED_TIMESTEP * FIXED_TIMESTEP;
const double MIN_TIME = 1.0 / MAX_FPS;
const double MAX_TIME = 1.0 / MIN_FPS;

// If you know templates or you want to give them a try/look
//---
template <typename T>
void linearInterpolationV(unsigned int length, T start[], T end[], T result[], float alpha)
{
    float omalpha = 1 - alpha;
    for (int i=0; i!=length; i++)
    {
        result[i] = static_cast<float>(start[i])*alpha + static_cast<float>(end[i])*omalpha;
    }
}
// if you have a known at compile time length this is better
template<typename T, unsigned int length>
inline void linearInterpolationV(T start[], T end[], T result[], float alpha)
{
    float omalpha = 1 - alpha;
    for (int i=0; i!=length; i++) // hopefully this loop will be unrolled
    {
        result[i] = static_cast<float>(start[i])*alpha + static_cast<float>(end[i])*omalpha;
    }
}
// use this with integers numbers
template<typename T, unsigned int length>
inline void linearInterpolationRoundV(T start[], T end[], T result[], float alpha)
{
    float omalpha = 1 - alpha;
    for (int i=0; i!=length; i++) // hopefully this loop will be unrolled
    {
        result[i] = round(static_cast<float>(start[i])*alpha + static_cast<float>(end[i])*omalpha);
    }
}

// If you don't know templates. These functions do the same as template<typename T, unsigned int length> linearInterpolationV( ...
//---
inline void linearInterpolationf4(float start[4], float end[4], float result[4], float alpha)
{
    float omalpha = 1 - alpha;
    for (int i=0; i!=4; i++) // hopefully this loop will be unrolled
    {
        result[i] = start[i]*alpha + end[i]*omalpha;
    }
}
inline void linearInterpolationf3(float start[3], float end[3], float result[3], float alpha)
{
    float omalpha = 1 - alpha;
    for (int i=0; i!=3; i++) // hopefully this loop will be unrolled
    {
        result[i] = start[i]*alpha + end[i]*omalpha;
    }
}
inline void linearInterpolationf2(float start[2], float end[2], float result[2], float alpha)
{
    float omalpha = 1 - alpha;
    for (int i=0; i!=2; i++) // hopefully this loop will be unrolled
    {
        result[i] = start[i]*alpha + end[i]*omalpha;
    }
}
inline void linearInterpolationf(float start, float end, float result, float alpha)
{
    float omalpha = 1 - alpha;
    result = start*alpha + end*omalpha;
}


struct Color
{
    Uint8 r,g,b,a;
    Color(Uint8 r=255,Uint8 g=255,Uint8 b=255,Uint8 a=255) : r(r),g(g),b(b),a(a){}
    void makeInterpolationOf(Color& a, Color& b, float alpha)
    {
        linearInterpolationRoundV<Uint8,4>(static_cast<Uint8*>(&a.r),static_cast<Uint8*>(&b.r),static_cast<Uint8*>(&r),alpha);
    }
};
struct PixPos
{
    int x,y;
    PixPos(int x=0, int y=0) : x(x),y(y){}
    void makeInterpolationOf(PixPos& a, PixPos& b, float alpha)
    {
        linearInterpolationRoundV<int,2>(static_cast<int*>(&a.x),static_cast<int*>(&b.x),static_cast<int*>(&x),alpha);
    }
};
struct Aspect
{
    Color color;
    PixPos pixel_position;
    Aspect(){};
    Aspect(Color&& _color, PixPos&& _pixel_position) : color(std::move(_color)), pixel_position(std::move(_pixel_position)) {};
    void makeInterpolationOf(Aspect& a, Aspect& b, float alpha)
    {
        color.makeInterpolationOf(a.color, b.color, alpha);
        pixel_position.makeInterpolationOf(a.pixel_position, b.pixel_position, alpha);
    }
};
struct PhysicsProperties
{
    struct Pos {float x,y;} pos;
    struct Vel {float x,y;} vel;
    struct Acc {float x,y;} acc;
    PhysicsProperties() : pos({0,0}), vel({0,0}), acc({0,0}){}
    PhysicsProperties(Pos& pos, Vel& vel, Acc& acc) : pos(pos), vel(vel), acc(acc){}
    PhysicsProperties(Pos&& _pos, Vel&& _vel, Acc&& _acc) : pos(std::move(_pos)), vel(std::move(_vel)), acc(std::move(_acc)){}
    void makeInterpolationOf(PhysicsProperties& a, PhysicsProperties& b, float alpha)
    {
        linearInterpolationV<float,4>(static_cast<float*>(&a.pos.x),static_cast<float*>(&b.pos.x),static_cast<float*>(&pos.x),alpha);
    }
    float totalAcc(Uint8 index)
    {
        // here we will calculate the total acceleration affecting the object; for example gravity, for now nothing.
        // (this probably could be function of pos, if so we have to separate the for cycle in stepAhead(...) )
        return 0;
    }
    // leapfrog integration
    void stepAhead(PhysicsProperties& prev_state)
    {
        // separate this cycle if totalAcc is in function of pos
        for (Uint8 i=0; i!=2; i++)
        {
            (&pos.x)[i] = (&prev_state.pos.x)[i] + (&prev_state.vel.x)[i]*FIXED_TIMESTEP + 0.5f*(&prev_state.acc.x)[i]*FIXED_TIMESTEP_2;
            (&acc.x)[i] = totalAcc(i);
            (&vel.x)[i] = (&prev_state.vel.x)[i] + 0.5f*((&acc.x)[i]+(&prev_state.acc.x)[i])*FIXED_TIMESTEP;
        }
    }
};

// an example of a working structure using the above ones
struct Square
{
    PhysicsProperties pp[3];
    Aspect aspect; // if aspect is in function of time or it's static we don't have to interpolate
    int px_side;
    // you should not touch these, so they should be made private and accessed through a method
    Uint8 currID;
    Uint8 prevID;
    const Uint8 renderID = 2;

    Square() : px_side(10), currID(0), prevID(1){}

    void fixedTSUpdate(double game_time)
    {
        std::swap(currID, prevID);
        pp[currID].stepAhead(pp[prevID]);
    }

    void smoothToRenderState(float alpha)
    {
        pp[renderID].makeInterpolationOf(pp[currID],pp[prevID],alpha);
    }

    void render(SDL_Renderer* renderer)
    {
        for (Uint8 i=0; i!=2; i++)
        {
            (&aspect.pixel_position.x)[i] = std::round((&pp[renderID].pos.x)[i]);
        }

        // Declare and initialize our rectangle
        SDL_Rect rect = {
                        aspect.pixel_position.x, aspect.pixel_position.y,   // int x,y: position of the top left corner
                        px_side, px_side};                                  // int w,h: width and height

        // set the color to draw with
        SDL_SetRenderDrawColor(
                            renderer,        // SDL_Renderer* renderer: the renderer to affect
                            aspect.color.r,  // Uint8 r: Red
                            aspect.color.g,  // Uint8 g: Green
                            aspect.color.b,  // Uint8 b: Blue
                            aspect.color.a); // Uint8 a: Alpha

        int ret;
        ret = SDL_RenderFillRect(
                                renderer,    // SDL_Renderer*   renderer: the renderer to affect
                                   &rect);   // const SDL_Rect* rect: the rectangle to fill);
        if (ret != 0)
        {
            const char *error = SDL_GetError();
            if (*error != '\0')
            {
                SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not draw a rect. SDL Error: %s at line #%d of file %s", error, __LINE__, __FILE__);
                SDL_ClearError();
            }
        }
    }

    void update(double game_time, double delta_time){};
};

void renderSolidText(TTF_Font *font, const char *text, const SDL_Color& color, SDL_Renderer *renderer, int x, int y)
{
    // Render some text in solid black to a new surface
    // then create a texture from it
    // then free the text surface

    int ret;

    SDL_Surface *text_surface;
    SDL_Texture *text_tex;
    // SDL_Log("n_steps %d", n_steps);
    // SDL_Log("clamped %fms", clamped_frame_duration*1000);
    // SDL_Log("real_fd %fms", real_fd*1000);
    // SDL_Log("real instant freq %fHz", 1.0/real_fd);
    // SDL_Log("frame_duration %fms", frame_duration*1000);
    // SDL_Log("instant freq %fHz", 1.0/frame_duration);
    if(!(text_surface=TTF_RenderText_Solid(font, text, color)))
    {
        // handle error here, perhaps print TTF_GetError at least
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_RenderText_Solid generated an error at line %d of file %s Error:%s\n",__LINE__,__FILE__,TTF_GetError());
    } else
    {

        SDL_Rect tex_size = {0,0,0,0};
        SDL_Rect tex_dest;

        tex_size.w = text_surface->w;
        tex_size.h = text_surface->h;
        tex_dest.w = tex_size.w;
        tex_dest.h = tex_size.h;
        tex_dest.x = x;
        tex_dest.y = y;


        text_tex = SDL_CreateTextureFromSurface(renderer, text_surface);
        if (!text_tex)
        {
            const char *error = SDL_GetError();
            if (*error != '\0')
            {
                SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not rendercopy. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
                SDL_ClearError();
            }
        }
        SDL_FreeSurface(text_surface);

        ret =
        SDL_RenderCopy( renderer,    // SDL_Renderer* renderer: the renderer to affect
                        text_tex,    // SDL_Texture* texture: the source texture
                        &tex_size,   // const SDL_Rect* srcrect: the source SDL_Rect structure or NULL for the entire texture  
                        &tex_dest);  // const SDL_Rect* dstrect: the destination SDL_Rect structure or NULL for the entire rendering target. The texture will be stretched to fill the given rectangle.
        if (ret != 0)
        {
            const char *error = SDL_GetError();
            if (*error != '\0')
            {
                SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not rendercopy. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
                SDL_ClearError();
            }
        }

        // Destroy the texture
        SDL_DestroyTexture(text_tex);
    }
}

namespace Colors
{
    const SDL_Color white = {255,255,255};
}

namespace AtomicFluid
{
    const Uint16 atom_side_px = 3;
    const Uint8 atom_max_q = 32;//(Uint8)-1;
    const Uint8 q_per_iter = 2;

    SDL_Renderer* renderer;
    TTF_Font *font;

    Color w_color = {0,0,128,255};
    Color s_color = {128,128,128,255};
    Color clear_color = {64,64,64,255};
    Color g_color = {0,0,0,255};

    bool render_grid = false;
    bool display_quantities = false;
    bool render_alpha = false;
    bool render_white = false;
    bool render_excess = true;

    namespace Types
    {
        const Uint8 AIR = 0x0;
        const Uint8 SOLID = 0x1;
        const Uint8 FLUID = 0x2;
    }

    // indices of the adjacency bitmask
    namespace Adj
    {
        const Uint8 TL = 0x1<<7;
        const Uint8 TC = 0x1<<6;
        const Uint8 TR = 0x1<<5;
        const Uint8 ML = 0x1<<4;
        const Uint8 MR = 0x1<<3;
        const Uint8 BL = 0x1<<2;
        const Uint8 BC = 0x1<<1;
        const Uint8 BR = 0x1<<0;
    }

    union Atom
    {
        Uint8 type;
        struct // fluid
        {
            Uint8 type_;
            Uint8 quantity;
        }; // fluid;
    };

    inline void mouseInGrid(int mx, int my, int & grid_x, int & grid_y, int offset_x=0, int offset_y=0)
    {
        grid_x = (mx-offset_x) / atom_side_px;
        grid_y = (my-offset_y) / atom_side_px;
    }

    void fixedTSGridUpdate(Atom * curr_grid, Atom * new_grid, Uint16 width, Uint16 height)
    {
#ifdef DEBUG
        // for debugging
        Uint64 total_fluid = 0;
#endif

        for (int y=0; y!=height; y++)
        {
            Uint16 Y = atom_side_px*y;
            for (int x=0; x!=width; x++)
            {
                Uint16 X = atom_side_px*x;
                Uint8 adj = 0x0;
                Atom &c_atom = curr_grid [y*width+x],
                     &n_atom = new_grid [y*width+x];
                Atom &c_bottom = curr_grid [(y+1)*width+x],
                     &c_top=curr_grid [(y-1)*width+x],
                     &c_left=curr_grid [y*width+(x-1)],
                     &c_right=curr_grid [y*width+(x+1)];
                Atom &n_bottom = new_grid [(y+1)*width+x],
                     &n_top=new_grid [(y-1)*width+x],
                     &n_left=new_grid [y*width+(x-1)],
                     &n_right=new_grid [y*width+(x+1)];
                switch (c_atom.type)
                {
                case Types::FLUID:
                 {
#ifdef DEBUG
                    total_fluid += c_atom.quantity;
#endif
                    if (y!=height-1) // bottom
                    {
                        if (x!=0) // left
                        {
                            switch (curr_grid[(y+1)*width+(x-1)].type)
                            {
                            case Types::SOLID:
                                adj |= Adj::BL;
                                break;
                            default:
                                break;
                            }
                        } else
                        {
                            adj |= Adj::BL;
                        }
                        // center
                        switch (c_bottom.type)
                        {
                        case Types::SOLID:
                            adj |= Adj::BC;
                            break;
                        default:
                            break;
                        }
                        if (x!=width-1) // right
                        {
                            switch (curr_grid[(y+1)*width+(x+1)].type)
                            {
                            case Types::SOLID:
                                adj |= Adj::BR;
                                break;
                            default:
                                break;
                            }
                        } else
                        {
                            adj |= Adj::BR;
                        }
                    } else
                    {
                        adj |= Adj::BL;
                        adj |= Adj::BC;
                        adj |= Adj::BR;
                    }

                    // middle

                    if (x!=0) // left
                    {
                        switch (c_left.type)
                        {
                        case Types::SOLID:
                            adj |= Adj::ML;
                            break;
                        default:
                            break;
                        }
                    } else
                    {
                        adj |= Adj::ML;
                    }
                    if (x!=width-1)
                    {
                        switch (c_right.type) // right
                        {
                        case Types::SOLID:
                            adj |= Adj::MR;
                            break;
                        default:
                            break;
                        }
                    } else
                    {
                        adj |= Adj::MR;
                    }

                    if (y!=0) // top
                    {
                        if (x!=0) // left
                        {
                            switch (curr_grid[(y-1)*width+(x-1)].type)
                            {
                            case Types::SOLID:
                                adj |= Adj::TL;
                                break;
                            default:
                                break;
                            }
                        } else
                        {
                            adj |= Adj::TL;
                        }
                        // center
                        switch (c_top.type)
                        {
                        case Types::SOLID:
                            adj |= Adj::TC;
                            break;
                        default:
                            break;
                        }
                        if (x!=width-1) // right
                        {
                            switch (curr_grid[(y-1)*width+(x+1)].type)
                            {
                            case Types::SOLID:
                                adj |= Adj::TR;
                                break;
                            default:
                                break;
                            }
                        } else
                        {
                            adj |= Adj::TR;
                        }
                    } else
                    {
                        adj |= Adj::TL;
                        adj |= Adj::TC;
                        adj |= Adj::TR;
                    }

                    // to manage excess
                    Uint8 new_quantity_old = n_atom.quantity;

                    if (c_bottom.type==Types::SOLID ||  // is solid [adj&Adj::BC]
                        (c_bottom.type==Types::FLUID    // or
                         &&                             // is fluid
                         c_bottom.quantity>=atom_max_q) // and full
                        )
                    {
                        Uint16 tot_l = c_left.type==Types::AIR ? 0 : adj&Adj::ML ? atom_max_q : c_left.quantity;
                        Uint16 tot_r = c_right.type==Types::AIR ? 0 : adj&Adj::MR ? atom_max_q : c_right.quantity;
                        bool both_sides_can_receive = 
                                                    (c_left.type==Types::AIR ||                       // is air
                                                    (c_left.type==Types::FLUID &&                     // or
                                                    c_left.quantity < c_atom.quantity &&// is smaller than this one
                                                    c_left.quantity < atom_max_q))                    // and is smaller than max
                                                    &&
                                                    (c_right.type==Types::AIR ||                       // is air
                                                    (c_right.type==Types::FLUID &&                     // or
                                                    c_right.quantity < c_atom.quantity &&// is smaller than this one
                                                    c_right.quantity < atom_max_q));                   // and is smaller than max
                        if (both_sides_can_receive
                            &&
                            tot_l<=atom_max_q-(q_per_iter>>1) // can contain all half quantity_per_iteration
                            &&
                            tot_r<=atom_max_q-(q_per_iter>>1) // can contain all half quantity_per_iteration
                            &&
                            c_atom.quantity >= q_per_iter
                            )
                        {
                            Uint8 lq = q_per_iter-(q_per_iter>>1);
                            Uint8 rq = q_per_iter>>1;

                            if (n_left.type!=Types::FLUID && lq!=0)
                            {
                                n_left.quantity = 0;
                                n_left.type = Types::FLUID;
                            }
                            if (n_right.type!=Types::FLUID && rq!=0)
                            {
                                n_right.quantity = 0;
                                n_right.type = Types::FLUID;
                            }
                            if (lq!=0)
                                n_left.quantity += lq;
                            if (rq!=0)
                                n_right.quantity += rq;

                            n_atom.quantity -= q_per_iter;
                        } else
                        if (both_sides_can_receive
                            &&
                            c_atom.quantity >= q_per_iter)
                        {
                            Uint8 lq = atom_max_q - tot_l;
                            Uint8 rq = atom_max_q - tot_r;
                            if (tot_l>tot_r)
                            {
                                rq = MIN(c_atom.quantity, MIN(q_per_iter-lq, rq)); // we are granted lq<q_per_iter
                                lq = MIN (c_atom.quantity-rq, lq);
                            } else
                            if (tot_l<tot_r)
                            {
                                lq = MIN(c_atom.quantity, MIN(q_per_iter-rq, lq)); // we are granted rq<q_per_iter
                                rq = MIN (c_atom.quantity-lq, rq);
                            }

                            if (n_left.type!=Types::FLUID && lq!=0)
                            {
                                n_left.quantity = 0;
                                n_left.type = Types::FLUID;
                            }
                            if (n_right.type!=Types::FLUID && rq!=0)
                            {
                                n_right.quantity = 0;
                                n_right.type = Types::FLUID;
                            }
                            if (lq!=0)
                                n_left.quantity += lq;
                            if (rq!=0)
                                n_right.quantity += rq;

                            n_atom.quantity -= lq+rq;
                        } else
                        if (both_sides_can_receive
                            &&
                            c_atom.quantity < q_per_iter)
                        {
                            Uint8 half_less = c_atom.quantity >> 1;
                            Uint8 half_more = c_atom.quantity - half_less;

                            Uint8 lq = half_less;
                            Uint8 rq = half_more;

                            if ((x+y)%2)
                                std::swap(lq,rq);

                            lq = MIN(atom_max_q - tot_l, lq);
                            rq = MIN(atom_max_q - tot_r, rq);

                            if (n_left.type!=Types::FLUID && lq!=0)
                            {
                                n_left.quantity = 0;
                                n_left.type = Types::FLUID;
                            }
                            if (n_right.type!=Types::FLUID && rq!=0)
                            {
                                n_right.quantity = 0;
                                n_right.type = Types::FLUID;
                            }
                            if (lq!=0)
                                n_left.quantity += lq;
                            if (rq!=0)
                                n_right.quantity += rq;

                            n_atom.quantity -= lq+rq;
                        } else
                        if (((c_left.type==Types::FLUID &&
                            (c_left.quantity >= c_atom.quantity || // is greater than this one
                            c_left.quantity >= atom_max_q))                      // or is greater than max
                                ||                                               // or
                            adj & Adj::ML                                        // is solid
                            )
                            &&
                            (c_right.type==Types::AIR ||                        // is air
                            (c_right.type==Types::FLUID &&                      // or
                            c_right.quantity < c_atom.quantity && // is smaller than this one
                            c_right.quantity < atom_max_q))                     // and smaller than max
                            )
                        {
                            Uint8 q = MIN(c_atom.quantity>>1, MIN(atom_max_q - tot_r, q_per_iter));

                            if (n_right.type!=Types::FLUID && q!=0)
                            {
                                n_right.quantity = 0;
                                n_right.type = Types::FLUID;
                            }

                            n_right.quantity += q;
                            n_atom.quantity -= q;
                        } else
                        if ((c_left.type==Types::AIR ||                        // is air
                            (c_left.type==Types::FLUID &&                      // or
                            c_left.quantity < c_atom.quantity && // is smaller than this one
                            c_left.quantity < atom_max_q))                     // and smaller than max
                            &&
                            (c_right.type==Types::FLUID &&
                            (c_right.quantity >= c_atom.quantity || // is greater than this one
                            c_right.quantity >= atom_max_q)                       // or is greater than max
                                ||                                                // or
                            adj & Adj::MR)                                        // is solid
                            )
                        {
                            Uint8 q = MIN(c_atom.quantity>>1, MIN(atom_max_q - tot_l, q_per_iter));
                            
                            if (n_left.type!=Types::FLUID && q!=0)
                            {
                                n_left.quantity = 0;
                                n_left.type = Types::FLUID;
                            }

                            n_left.quantity += q;
                            n_atom.quantity -= q;
                        }
                    } else
                    if (c_bottom.type==Types::AIR ||   // is iar
                        (c_bottom.type==Types::FLUID   // or
                        &&                             // is fluid
                        c_bottom.quantity<atom_max_q)  // and not full
                        )
                    {
                        Uint8 bq = atom_max_q;
                        if (c_bottom.type==Types::FLUID)
                        {
                            bq -= (c_bottom.quantity);
                        }
                        bq = MIN(c_atom.quantity, MIN (q_per_iter, bq));

                        if ((c_left.type==Types::FLUID &&
                            c_left.quantity >= c_atom.quantity)   // is greater than this one
                            &&
                            (c_right.type==Types::AIR ||                        // is air
                            (c_right.type==Types::FLUID &&                      // or
                            c_right.quantity < c_atom.quantity && // is smaller than this one
                            c_right.quantity < atom_max_q))                     // and is smaller than max
                            )
                        {
                            if (bq > q_per_iter - q_per_iter>>1)
                            {
                                Uint8 rq = q_per_iter - q_per_iter>>1;
                                bq -= rq;

                                if (rq && n_right.type!=Types::FLUID)
                                {
                                    n_right.quantity = 0;
                                    n_right.type = Types::FLUID;
                                }

                                n_right.quantity += rq;
                                n_atom.quantity -= rq;
                            }
                        } else
                        if ((c_left.type==Types::AIR ||                        // is air
                            (c_left.type==Types::FLUID &&                      // or
                            c_left.quantity < c_atom.quantity && // is smaller than this one
                            c_left.quantity < atom_max_q))                     // and is smaller than max
                            &&
                            (c_right.type==Types::FLUID &&
                            c_right.quantity >= c_atom.quantity)  // is greater than this one
                            )
                        {
                            if (bq > q_per_iter - q_per_iter>>1)
                            {
                                Uint8 lq = q_per_iter - q_per_iter>>1;
                                bq -= lq;

                                if (lq && n_left.type!=Types::FLUID)
                                {
                                    n_left.quantity = 0;
                                    n_left.type = Types::FLUID;
                                }

                                n_left.quantity += lq;
                                n_atom.quantity -= lq;
                            }
                        }
                        if (bq!=0 && n_bottom.type!=Types::FLUID)
                        {
                            n_bottom.quantity = 0;
                            n_bottom.type = Types::FLUID;
                        }

                        n_bottom.quantity += bq;
                        n_atom.quantity -= bq;
                    }

                    // TODO check top+bottom in adj cells

                    // manage empty fluid

                    if (n_atom.quantity == 0)
                    {
                        n_atom.type = Types::AIR;
                    }

                    // manage excess

                    Uint8 lost = new_quantity_old - n_atom.quantity;
                    Uint8 curr_est = c_atom.quantity - lost;

                #ifdef DEBUG
                    if (curr_est > c_atom.quantity)
                    {
                        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "overflow!!! line %d", __LINE__);
                        exit(1);
                    }
                #endif

                    if (curr_est > atom_max_q)
                    {
                        Uint8 q = n_atom.quantity - atom_max_q;
                        if ((! (adj & Adj::TC))
                            &&
                            (n_top.type==Types::AIR || 
                            (n_top.type==Types::FLUID && n_top.quantity + q <= static_cast<Uint8>(-1)))
                            )
                        {

                            if (n_top.quantity + q > n_top.quantity)
                            {
                                if (n_top.type!=Types::FLUID)
                                {
                                    n_top.quantity = 0;
                                    n_top.type = Types::FLUID;
                                }
                                Uint16 t = n_top.quantity + n_atom.quantity;
                                Uint8 oldtop = n_top.quantity;
                                Uint8 oldmy = n_atom.quantity;

                                if (n_top.quantity + q > static_cast<Uint8>(-1))
                                {
                                    // SDL_Log("%u+%u=%u",n_top.quantity, q, n_top.quantity + q );
                                    // SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "overflow!!! line %d", __LINE__);
                                    // exit(1);
                                } else
                                {

                                    n_top.quantity += q;
                                    // if (n_top.quantity > atom_max_q)
                                    // {
                                    //     q -= n_top.quantity - atom_max_q;
                                    //     n_top.quantity = atom_max_q;
                                    // }

                                #ifdef DEBUG
                                    if (n_atom.quantity < q)
                                    {
                                        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "overflow!!! line %d", __LINE__);
                                        exit(1);
                                    }
                                #endif

                                    n_atom.quantity -= q;

                                #ifdef DEBUG
                                    if (t != n_top.quantity + n_atom.quantity)
                                    {
                                        SDL_Log("oldtot:%u newtot:%u oldtopq:%u oldmy:%u topq:%u myw:%u q:%u",
                                                        t,
                                                        n_top.quantity + n_atom.quantity,
                                                        oldtop,
                                                        oldmy,  
                                                        n_top.quantity,
                                                        n_atom.quantity,
                                                        q);
                                        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "overflow!!! line %d", __LINE__);
                                        exit(1);
                                    }
                                #endif
                                }
                            } else
                            {
                                SDL_Log("overflow at line %d",__LINE__);
                            }
                        } else
                        // if (false)
                        {
                            if (!(adj & Adj::ML) && !(adj & Adj::MR) )
                            {
                                if (curr_est > atom_max_q + 1      // exceed at least of 2 
                                    &&                             // and
                                    (n_left.type==Types::AIR ||     // left is air
                                    (n_left.type==Types::FLUID &&   // or
                                    n_left.quantity < curr_est))    // is smaller
                                    &&                             // and
                                    (n_right.type==Types::AIR ||    // right is air
                                    (n_right.type==Types::FLUID &&  // or
                                    n_right.quantity < curr_est))   // is smaller
                                    )
                                {
                                    if (n_left.type!=Types::FLUID)
                                    {
                                        n_left.quantity = 0;
                                        n_left.type = Types::FLUID;
                                    }
                                    if (n_right.type!=Types::FLUID)
                                    {
                                        n_right.quantity = 0;
                                        n_right.type = Types::FLUID;
                                    }

                                    Uint8 q = n_atom.quantity - atom_max_q;
                                    Uint8 pq;
                                    if (n_left.quantity < n_right.quantity)
                                    {
                                        pq = n_right.quantity - n_left.quantity;

                                        if (pq < q)
                                        {
                                            n_left.quantity += pq;
                                        } else
                                        {
                                            n_left.quantity += q;
                                        }
                                    } else
                                    {
                                        pq = n_left.quantity - n_right.quantity;
                                        
                                        if (pq < q)
                                        {
                                            n_right.quantity += pq;
                                        } else
                                        {
                                            n_right.quantity += q;
                                        }
                                    }

                                    n_atom.quantity = atom_max_q;
                                    
                                    if (pq < q)
                                    {
                                        q -= pq;
                                        Uint8 half_less = q >> 1;
                                        Uint8 half_more = q - half_less;

                                        Uint8 lq = half_less;
                                        Uint8 rq = half_more;

                                        if ((x+y)%2)
                                            std::swap(lq,rq);

                                        if (static_cast<int>(n_left.quantity) + static_cast<int>(lq) > static_cast<Uint8>(-1))
                                        {
                                        #ifdef DEBUG
                                            SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "overflow!!! line %d %u+%u", __LINE__,n_left.quantity,lq);
                                        #endif
                                            Uint8 of = n_left.quantity + lq +1;
                                        #ifdef DEBUG
                                            SDL_Log("of=%u",of);
                                        #endif
                                            n_atom.quantity += of;
                                            n_left.quantity = static_cast<Uint8>(-1);
                                        } else
                                            n_left.quantity += lq;

                                        if (static_cast<int>(n_right.quantity) + static_cast<int>(rq) >  static_cast<Uint8>(-1))
                                        {
                                        #ifdef DEBUG
                                            SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "overflow!!! line %d %u+%u", __LINE__,n_right.quantity,rq);
                                        #endif
                                            Uint8 of = n_right.quantity + rq +1;
                                        #ifdef DEBUG
                                            SDL_Log("of=%u",of);
                                        #endif
                                            n_atom.quantity += of;
                                            n_right.quantity = static_cast<Uint8>(-1);
                                        } else
                                            n_right.quantity += rq;
                                    }


                                    // Uint16 avg = n_left.quantity +
                                    //              n_atom.quantity +
                                    //              n_right.quantity;
                                    
                                    // if (avg % 3)
                                    // {
                                    //     avg = avg/3+1;
                                    // } else
                                    // {
                                    //     avg = avg/3;
                                    // }

                                    // Uint8 to_lose = 0;
                                    // if (n_left.quantity < avg)
                                    // {
                                    //     to_lose += avg - n_left.quantity;
                                    //     n_left.quantity = avg;
                                    // }
                                    // if (n_right.quantity < avg)
                                    // {
                                    //     to_lose += avg - n_right.quantity;
                                    //     n_right.quantity = avg;
                                    // }
                                    // n_atom.quantity -= to_lose;
                                }
                            } else
                            if (!(adj & Adj::MR))
                            {
                                if (n_right.type==Types::AIR ||                 // right is air
                                    (n_right.type==Types::FLUID &&              // or
                                    n_right.quantity != static_cast<Uint8>(-1)) // not var-max
                                    // n_right.quantity < curr_est))               // is smaller
                                    )
                                {
                                    if (n_right.type!=Types::FLUID)
                                    {
                                        n_right.quantity = 0;
                                        n_right.type = Types::FLUID;
                                    }
                                    
                                    n_atom.quantity = atom_max_q;

                                    if (static_cast<int>(n_right.quantity) + static_cast<int>(q) >  static_cast<Uint8>(-1))
                                    {
                                    #ifdef DEBUG
                                        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "overflow!!! line %d %u+%u", __LINE__,n_right.quantity,q);
                                    #endif
                                        Uint8 of = n_right.quantity + q +1;
                                    #ifdef DEBUG
                                        SDL_Log("of=%u",of);
                                    #endif
                                        n_atom.quantity += of;
                                        n_right.quantity = static_cast<Uint8>(-1);
                                    } else
                                        n_right.quantity += q;

                                    // Uint16 avg = n_atom.quantity+n_right.quantity;
                                    
                                    // if (avg % 2)
                                    // {
                                    //     avg = avg/2+1;
                                    // } else
                                    // {
                                    //     avg = avg/2;
                                    // }
                                    
                                    // Uint8 to_lose = 0;
                                    // if (n_right.quantity < avg)
                                    // {
                                    //     to_lose += avg - n_right.quantity;
                                    //     n_right.quantity = avg;
                                    // }
                                    // n_atom.quantity -= to_lose;
                                }
                            } else
                            if (!(adj & Adj::ML))
                            {
                                if (n_left.type==Types::AIR ||                  // left is air
                                    (n_left.type==Types::FLUID &&               // or
                                    n_left.quantity != static_cast<Uint8>(-1))  // not var-max
                                    // n_left.quantity < curr_est))                // is smaller
                                    )
                                {
                                    if (n_left.type!=Types::FLUID)
                                    {
                                        n_left.quantity = 0;
                                        n_left.type = Types::FLUID;
                                    }
                                    
                                    n_atom.quantity = atom_max_q;

                                    if (static_cast<int>(n_left.quantity) + static_cast<int>(q) >  static_cast<Uint8>(-1))
                                    {
                                    #ifdef DEBUG
                                        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "overflow!!! line %d %u+%u", __LINE__,n_left.quantity,q);
                                    #endif
                                        Uint8 of = n_left.quantity + q +1;
                                    #ifdef DEBUG
                                        SDL_Log("of=%u",of);
                                    #endif
                                        n_atom.quantity += of;
                                        n_left.quantity = static_cast<Uint8>(-1);
                                    } else
                                        n_left.quantity += q;

                                    // Uint16 avg = n_left.quantity+n_atom.quantity;
                                    
                                    // if (avg % 2)
                                    // {
                                    //     avg = avg/2+1;
                                    // } else
                                    // {
                                    //     avg = avg/2;
                                    // }
                                    
                                    // Uint8 to_lose = 0;
                                    // if (n_left.quantity < avg)
                                    // {
                                    //     to_lose += avg - n_left.quantity;
                                    //     n_left.quantity = avg;
                                    // }
                                    // n_atom.quantity -= to_lose;
                                }
                            } else
                            {
                                
                            }
                        }
                    }

                    break; // Type::FLUID
                 }

                default:
                    break;
                }
            }
        }

#ifdef DEBUG
        SDL_Log("total fluid: %lu",total_fluid);
#endif
    }

    void renderGrid(Atom * grid, Uint16 width, Uint16 height, int px_x_offset=0, int px_y_offset=0)
    {
        int ret;
        if (render_alpha || render_white || render_excess)
        {
            ret = SDL_SetRenderDrawBlendMode( renderer,
                                                  SDL_BLENDMODE_BLEND);
            if (ret != 0)
            {
                const char *error = SDL_GetError();
                if (*error != '\0')
                {
                    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not set SDL blend mode. SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__);
                    SDL_ClearError();
                }
            }
        }

        for (int y=0; y!=height; y++)
        {
            Uint16 Y = px_y_offset+atom_side_px*y;
            for (int x=0; x!=width; x++)
            {
                Uint16 X = px_x_offset+atom_side_px*x;
                Atom atom = grid[y*width+x];
                switch (atom.type)
                {

                case Types::FLUID:


                    if (!render_alpha)
                    {
                        // set the color to draw with
                        ret = SDL_SetRenderDrawColor(
                                            renderer,   // SDL_Renderer* renderer: the renderer to affect
                                            w_color.r,  // Uint8 r: Red
                                            w_color.g,  // Uint8 g: Green
                                            w_color.b,  // Uint8 b: Blue
                                            w_color.a); // Uint8 a: Alpha
                        if (ret != 0)
                        {
                            const char *error = SDL_GetError();
                            if (*error != '\0')
                            {
                                SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not set SDL render color. SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__);
                                SDL_ClearError();
                            }
                        }
                    }

                    if (grid[y*width+x].quantity)
                    {
                        if (render_alpha)
                        {
                            float a = static_cast<float>(MIN(atom_max_q, grid[y*width+x].quantity)) / static_cast<float>(atom_max_q);

                            // set the color to draw with
                            ret = SDL_SetRenderDrawColor(
                                                renderer,   // SDL_Renderer* renderer: the renderer to affect
                                                w_color.r,  // Uint8 r: Red
                                                w_color.g,  // Uint8 g: Green
                                                w_color.b,  // Uint8 b: Blue
                                                std::round(a*255.0));     // Uint8 a: Alpha
                            if (ret != 0)
                            {
                                const char *error = SDL_GetError();
                                if (*error != '\0')
                                {
                                    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not set SDL render color. SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__);
                                    SDL_ClearError();
                                }
                            }
                            SDL_Rect rect = {
                                    X, Y,   // int x,y: position of the top left corner
                                    atom_side_px, atom_side_px};      // int w,h: width and height
                            SDL_RenderFillRect(renderer, &rect);
                        } else
                        if (render_white)
                        {
                            float a = static_cast<float>(MIN(atom_max_q, grid[y*width+x].quantity)) / static_cast<float>(atom_max_q);

                            // set the color to draw with
                            ret = SDL_SetRenderDrawColor(
                                                renderer,   // SDL_Renderer* renderer: the renderer to affect
                                                (w_color.r * a) + (250 * (1-a)),  // Uint8 r: Red
                                                (w_color.g * a) + (250 * (1-a)),  // Uint8 g: Green
                                                (w_color.b * a) + (255 * (1-a)),  // Uint8 b: Blue
                                                (w_color.a * a) + (128 * (1-a))); // Uint8 a: Alpha
                            if (ret != 0)
                            {
                                const char *error = SDL_GetError();
                                if (*error != '\0')
                                {
                                    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not set SDL render color. SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__);
                                    SDL_ClearError();
                                }
                            }
                            SDL_Rect rect = {
                                    X, Y,   // int x,y: position of the top left corner
                                    atom_side_px, atom_side_px};      // int w,h: width and height
                            SDL_RenderFillRect(renderer, &rect);
                        } else
                        if (render_excess)
                        {
                            float a = static_cast<float>(MIN(atom_max_q, grid[y*width+x].quantity)) / static_cast<float>(atom_max_q);
                            float e = grid[y*width+x].quantity<=atom_max_q ? 0 : static_cast<float>(grid[y*width+x].quantity-atom_max_q) / static_cast<float>(atom_max_q);

                            if (e==0)
                            {
                                // set the color to draw with
                                ret = SDL_SetRenderDrawColor(
                                                    renderer,   // SDL_Renderer* renderer: the renderer to affect
                                                    (w_color.r * a) + (250 * (1-a)),  // Uint8 r: Red
                                                    (w_color.g * a) + (250 * (1-a)),  // Uint8 g: Green
                                                    (w_color.b * a) + (255 * (1-a)),  // Uint8 b: Blue
                                                    (w_color.a * a) + (128 * (1-a))); // Uint8 a: Alpha
                            } else
                            if (e<=1)
                            {
                                e = 1+std::log(e);
                                // set the color to draw with
                                ret = SDL_SetRenderDrawColor(
                                                    renderer,   // SDL_Renderer* renderer: the renderer to affect
                                                    w_color.r * (1-e) + 255 * e,  // Uint8 r: Red
                                                    w_color.g * (1-e) + 0 *   e,    // Uint8 g: Green
                                                    w_color.b * (1-e) + 0 *   e,    // Uint8 b: Blue
                                                    w_color.a * (1-e) + 255 * e); // Uint8 a: Alpha
                            } else
                            if (e<=2)
                            {
                                e = 1+std::log(e-2);
                                // set the color to draw with
                                ret = SDL_SetRenderDrawColor(
                                                    renderer,   // SDL_Renderer* renderer: the renderer to affect
                                                    w_color.r * (1-e) + 0 *   e,  // Uint8 r: Red
                                                    w_color.g * (1-e) + 255 * e,  // Uint8 g: Green
                                                    w_color.b * (1-e) + 0 *   e,  // Uint8 b: Blue
                                                    w_color.a * (1-e) + 255 * e); // Uint8 a: Alpha
                            } else
                            // if (e<=3)
                            {
                                e = 1+std::log(e-3);
                                // set the color to draw with
                                ret = SDL_SetRenderDrawColor(
                                                    renderer,   // SDL_Renderer* renderer: the renderer to affect
                                                    w_color.r * (1-e) + 255 * e,  // Uint8 r: Red
                                                    w_color.g * (1-e) + 255 * e,  // Uint8 g: Green
                                                    w_color.b * (1-e) + 0   * e,  // Uint8 b: Blue
                                                    w_color.a * (1-e) + 255 * e); // Uint8 a: Alpha
                            }
                            if (ret != 0)
                            {
                                const char *error = SDL_GetError();
                                if (*error != '\0')
                                {
                                    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not set SDL render color. SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__);
                                    SDL_ClearError();
                                }
                            }
                            SDL_Rect rect = {
                                    X, Y,   // int x,y: position of the top left corner
                                    atom_side_px, atom_side_px};      // int w,h: width and height
                            SDL_RenderFillRect(renderer, &rect);
                        } else
                        {
                            int ah = MIN(atom_max_q, grid[y*width+x].quantity);
                            ah = ah*atom_side_px / atom_max_q;
                            SDL_Rect rect = {
                                    X, Y+atom_side_px-ah,   // int x,y: position of the top left corner
                                    atom_side_px, ah};      // int w,h: width and height
                            SDL_RenderFillRect(renderer, &rect);
                        }
                    }
                    if (display_quantities)
                    {
                        char text[1024];
                        sprintf(text, "%2d",grid[y*width+x].quantity);
                        renderSolidText(font, text, Colors::white, renderer, X+2, Y+atom_side_px/4);
                    }

                    break;

                case Types::SOLID:

                    {
                    // set the color to draw with
                    SDL_SetRenderDrawColor(
                                        renderer,   // SDL_Renderer* renderer: the renderer to affect
                                        s_color.r,  // Uint8 r: Red
                                        s_color.g,  // Uint8 g: Green
                                        s_color.b,  // Uint8 b: Blue
                                        s_color.a); // Uint8 a: Alpha
                    SDL_Rect rect = {
                            X, Y,   // int x,y: position of the top left corner
                            atom_side_px, atom_side_px};      // int w,h: width and height
                    SDL_RenderFillRect(renderer, &rect);
                    }
                    break;

                default:
                    break;
                }
            }
        }

        if (render_grid)
        {
            // set the color to draw with
            SDL_SetRenderDrawColor(
                                renderer,        // SDL_Renderer* renderer: the renderer to affect
                                g_color.r,  // Uint8 r: Red
                                g_color.g,  // Uint8 g: Green
                                g_color.b,  // Uint8 b: Blue
                                g_color.a); // Uint8 a: Alpha
            for (int y=0; y!=height; y++)
            {
                Uint16 Y = px_y_offset+atom_side_px*y;
                for (int x=0; x!=width; x++)
                {
                    Uint16 X = px_x_offset+atom_side_px*x;
                    SDL_Rect rect = {
                            X, Y,   // int x,y: position of the top left corner
                            atom_side_px, atom_side_px};      // int w,h: width and height
                    SDL_RenderDrawRect(renderer, &rect);
                }

            }
        }
    }
}


double getTimeInSec()
{
    static double coeff;
    static bool isInitialized;
    if (!isInitialized) {
        isInitialized = true;
        Uint64 freq = SDL_GetPerformanceFrequency();
        coeff = 1.0 / (double)freq;
    }
    Uint64 val = SDL_GetPerformanceCounter();

    return (double)val * coeff;
}


int main(int argc, char** argv)
{
    // SDL version
    SDL_version SDL_compiled;
    SDL_version SDL_linked;

    SDL_VERSION(&SDL_compiled);
    SDL_GetVersion(&SDL_linked);
    printf("Compiled against SDL version %d.%d.%d\n",
           SDL_compiled.major, SDL_compiled.minor, SDL_compiled.patch);
    printf("Linking against SDL version %d.%d.%d\n",
           SDL_linked.major, SDL_linked.minor, SDL_linked.patch);

    // SDL_ttf version
    SDL_version SDL_ttf_compiled;
    const SDL_version *SDL_ttf_linked=TTF_Linked_Version();
    SDL_TTF_VERSION(&SDL_ttf_compiled);
    printf("Compiled against SDL_ttf version: %d.%d.%d\n", 
            SDL_ttf_compiled.major, SDL_ttf_compiled.minor, SDL_ttf_compiled.patch);
    printf("Linking against SDL_ttf version: %d.%d.%d\n", 
            SDL_ttf_linked->major, SDL_ttf_linked->minor, SDL_ttf_linked->patch);


#ifdef DEBUG
    SDL_LogSetAllPriority(SDL_LOG_PRIORITY_WARN);
#endif

    SDL_Window *window; // Declare a pointer to an SDL_Window
    SDL_Renderer *renderer = NULL;
    int ret;

    // Initialize
    //---

    // Initialize SDL_ttf
    if(TTF_Init()==-1)
    {
        std::cerr<<"TTF_Init: "<<TTF_GetError()<<std::endl;
        return 1;
    }

    // Initialize SDL's Video subsystem
    ret = SDL_Init(SDL_INIT_VIDEO);
    if (ret != 0)
    {
        const char *error = SDL_GetError();
        if (*error != '\0')
        {
            SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not SDL_Init. SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__);
            // SDL_ClearError();
            return 1;
        }
    }

    // Create an application window with the following settings:
    window = SDL_CreateWindow(
                        "SDL2 LibreLab Workshop - Lecture 5",    // const char* title
                        SDL_WINDOWPOS_UNDEFINED,                 // int x: initial x position, SDL_WINDOWPOS_CENTERED, or SDL_WINDOWPOS_UNDEFINED
                        SDL_WINDOWPOS_UNDEFINED,                 // int y: initial y position, SDL_WINDOWPOS_CENTERED, or SDL_WINDOWPOS_UNDEFINED
                        800,                                     // int w: width, in pixels
                        800,                                     // int h: height, in pixels
                        SDL_WINDOW_SHOWN                         // Uint32 flags: window options, see docs
            );

    // Check that the window was successfully made
    if(window==NULL)
    {
        const char *error = SDL_GetError();
        if (*error != '\0')
        {
            // In the event that the window could not be made...
            SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not create window. SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__);
            SDL_Quit();
            return 1;
        }
    }

    // Create accelerated renderer
    renderer = SDL_CreateRenderer(
                                window,                       // SDL_Window* window: the window where rendering is displayed
                                -1,                           // int index: the index of the rendering driver to initialize, or -1 to initialize the first one supporting the requested flags
                                SDL_RENDERER_ACCELERATED);    // Uint32 flags: 0, or one or more SDL_RendererFlags OR'd together;


    // Program life cycle
    //---

    SDL_Event event;
    bool running = true;
    double old_time = getTimeInSec();
    double game_time = 0, alpha, time_accumulator;
    int n_steps;


    // load font .ttf at size 16 into font
    TTF_Font *font;
    font = TTF_OpenFont("fonts/Inconsolata/Inconsolata-Regular.ttf",    // const char *file: File name to load font from.
                                                                12);    // int ptsize: Point size (based on 72DPI) to load font as. This basically translates to pixel height.
    if(!font)
    {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_OpenFont: %s\n", TTF_GetError());
        return 2;
    }

    // Atom grid initialization
    const Uint16 gside = 250;
    Uint8 currID = 0,
          newID  = 1;
    AtomicFluid::Atom *grid[2];
    grid[0] = new AtomicFluid::Atom[gside*gside]();
    grid[1] = new AtomicFluid::Atom[gside*gside]();

    int grid_offset_x = 0;
    int grid_offset_y = 32;

    std::default_random_engine generator(100);//std::chrono::system_clock::now().time_since_epoch().count());
    std::uniform_int_distribution<int> distribution(1,6);
    //int dice_roll = distribution(generator);  // generates number in the range 1..6

    for (int i=0; i!=gside; i++)
        for (int j=0; j!=gside; j++)
        {
            int r;
            r = distribution(generator);
            //r = -1;
            for (int b=0; b!=2; b++)
            {
                grid[b][i*gside+j].type = AtomicFluid::Types::AIR;
                // grid[i*gside+j].type = ((!((j+i)%3)&&i%2)||i==gside-1||j==0||j==gside-1) ? AtomicFluid::Types::SOLID : AtomicFluid::Types::FLUID;
                grid[b][i*gside+j].type = (i==gside-1||j==0||j==gside/2||j==gside-1||(r%6)==0) ?
                                                AtomicFluid::Types::SOLID
                                                :
                                                (i==gside-2||(i==gside-3&&(j==gside/4||j==gside/4+1)) || (r%6)>3) ?
                                                    AtomicFluid::Types::FLUID
                                                    :
                                                    AtomicFluid::Types::AIR;


                if (grid[b][i*gside+j].type == AtomicFluid::Types::FLUID)
                {
                    grid[b][i*gside+j].quantity = i==gside-2?32:32;
                }

                grid[b][9*gside+gside/2+1].type = AtomicFluid::Types::SOLID;
                grid[b][9*gside+gside/2+3].type = AtomicFluid::Types::SOLID;
                grid[b][10*gside+gside/2+1].type = AtomicFluid::Types::FLUID;
                grid[b][10*gside+gside/2+1].quantity = 32;
                grid[b][10*gside+gside/2+3].type = AtomicFluid::Types::FLUID;
                grid[b][10*gside+gside/2+3].quantity = 32;
                grid[b][10*gside+gside/2+4].type = AtomicFluid::Types::SOLID;
                grid[b][11*gside+gside/2+1].type = AtomicFluid::Types::SOLID;
                grid[b][11*gside+gside/2+3].type = AtomicFluid::Types::SOLID;
                grid[b][12*gside+gside/2+1].type = AtomicFluid::Types::SOLID;
                grid[b][12*gside+gside/2+2].type = AtomicFluid::Types::SOLID;
                grid[b][12*gside+gside/2+3].type = AtomicFluid::Types::SOLID;
                grid[b][12*gside+gside/2+4].type = AtomicFluid::Types::SOLID;


                grid[b][9*gside+gside/2+1+4].type = AtomicFluid::Types::SOLID;
                grid[b][9*gside+gside/2+3+4].type = AtomicFluid::Types::SOLID;
                grid[b][10*gside+gside/2+1+4].type = AtomicFluid::Types::FLUID;
                grid[b][10*gside+gside/2+1+4].quantity = 32;
                grid[b][10*gside+gside/2+2+4].type = AtomicFluid::Types::FLUID;
                grid[b][10*gside+gside/2+2+4].quantity = 16;
                grid[b][10*gside+gside/2+4+4].type = AtomicFluid::Types::SOLID;
                grid[b][11*gside+gside/2+1+4].type = AtomicFluid::Types::SOLID;
                grid[b][11*gside+gside/2+3+4].type = AtomicFluid::Types::SOLID;
                grid[b][12*gside+gside/2+1+4].type = AtomicFluid::Types::SOLID;
                grid[b][12*gside+gside/2+2+4].type = AtomicFluid::Types::SOLID;
                grid[b][12*gside+gside/2+3+4].type = AtomicFluid::Types::SOLID;
                grid[b][12*gside+gside/2+4+4].type = AtomicFluid::Types::SOLID;

                grid[b][13*gside+gside/2+1].type = AtomicFluid::Types::SOLID;
                grid[b][13*gside+gside/2+3].type = AtomicFluid::Types::SOLID;
                grid[b][13*gside+gside/2+2].type = AtomicFluid::Types::FLUID;
                grid[b][13*gside+gside/2+2].quantity = 32;
                grid[b][14*gside+gside/2+4].type = AtomicFluid::Types::SOLID;
                grid[b][15*gside+gside/2+1].type = AtomicFluid::Types::SOLID;
                grid[b][15*gside+gside/2+3].type = AtomicFluid::Types::SOLID;
                grid[b][16*gside+gside/2+1].type = AtomicFluid::Types::SOLID;
                grid[b][16*gside+gside/2+2].type = AtomicFluid::Types::SOLID;
                grid[b][16*gside+gside/2+3].type = AtomicFluid::Types::SOLID;
                grid[b][16*gside+gside/2+4].type = AtomicFluid::Types::SOLID;

                grid[b][13*gside+gside/2+1+4].type = AtomicFluid::Types::SOLID;
                grid[b][13*gside+gside/2+3+4].type = AtomicFluid::Types::SOLID;
                grid[b][13*gside+gside/2+2+4].type = AtomicFluid::Types::FLUID;
                grid[b][13*gside+gside/2+2+4].quantity = 16;
                grid[b][14*gside+gside/2+4+4].type = AtomicFluid::Types::SOLID;
                grid[b][15*gside+gside/2+1+4].type = AtomicFluid::Types::SOLID;
                grid[b][15*gside+gside/2+3+4].type = AtomicFluid::Types::SOLID;
                grid[b][16*gside+gside/2+1+4].type = AtomicFluid::Types::SOLID;
                grid[b][16*gside+gside/2+2+4].type = AtomicFluid::Types::SOLID;
                grid[b][16*gside+gside/2+3+4].type = AtomicFluid::Types::SOLID;
                grid[b][16*gside+gside/2+4+4].type = AtomicFluid::Types::SOLID;
            }
        }

    AtomicFluid::renderer = renderer;
    AtomicFluid::font = font;

    // editor
    AtomicFluid::Atom atoms_palette[3];
    atoms_palette[0].type = AtomicFluid::Types::SOLID;
    atoms_palette[1].type = AtomicFluid::Types::AIR;
    atoms_palette[2].type = AtomicFluid::Types::FLUID;
    atoms_palette[2].quantity = AtomicFluid::atom_max_q;
    Uint8 selected_atom_palette = 0;
    char atoms_palette_names [3][6] = {"SOLID", "AIR", "FLUID"};
    struct
    {
        bool left=0, right=0;
        int x=0,y=0;
    } mouse;
    bool insertion_locked = false;

    // get the loaded font's style
    {
    int style;
    style = TTF_GetFontStyle(font);
    std::cout<<"The font style is:";
    if(style==TTF_STYLE_NORMAL)
        std::cout<<" normal";
    else {
        if(style&TTF_STYLE_BOLD)
            std::cout<<" bold";
        if(style&TTF_STYLE_ITALIC)
            std::cout<<" italic";
        if(style&TTF_STYLE_UNDERLINE)
            std::cout<<" underline";
        if(style&TTF_STYLE_STRIKETHROUGH)
            std::cout<<" strikethrough";
    }
    std::cout<<std::endl;
    }
    
    // set the loaded font's style to bold italics
    TTF_SetFontStyle(font, TTF_STYLE_NORMAL);

    // get the loaded font's style
    {
    int style;
    style = TTF_GetFontStyle(font);
    std::cout<<"The font style is:";
    if(style==TTF_STYLE_NORMAL)
        std::cout<<" normal";
    else {
        if(style&TTF_STYLE_BOLD)
            std::cout<<" bold";
        if(style&TTF_STYLE_ITALIC)
            std::cout<<" italic";
        if(style&TTF_STYLE_UNDERLINE)
            std::cout<<" underline";
        if(style&TTF_STYLE_STRIKETHROUGH)
            std::cout<<" strikethrough";
    }
    std::cout<<std::endl;
    }

    {
    // get the loaded font's outline width
    int outline=TTF_GetFontOutline(font);
    SDL_Log("The font outline width is %d pixels\n",outline);
    }
    
    // set the loaded font's outline to 1 pixel wide
    TTF_SetFontOutline(font, 0);
    
    {
    // get the loaded font's outline width
    int outline=TTF_GetFontOutline(font);
    SDL_Log("The font outline width is %d pixels\n",outline);
    }

    {
    // get the loaded font's hinting setting
    int hinting=TTF_GetFontHinting(font);
    SDL_Log("The font hinting is currently set to %s\n",
            hinting==TTF_HINTING_NORMAL?"Normal":
            hinting==TTF_HINTING_LIGHT?"Light":
            hinting==TTF_HINTING_MONO?"Mono":
            hinting==TTF_HINTING_NONE?"None":
            "Unknonwn");
    }

    // set the loaded font's hinting to optimized for monochrome rendering
    TTF_SetFontHinting(font, TTF_HINTING_MONO);
    // set the loaded font's hinting to optimized for monochrome rendering
    TTF_SetFontHinting(font, TTF_HINTING_NORMAL);

    {
    // get the loaded font's kerning setting
    int kerning = TTF_GetFontKerning(font);
    SDL_Log("The font kerning is currently set to %s\n",
            kerning==0?"Off":"On");
    }
    
    // turn off kerning on the loaded font
    TTF_SetFontKerning(font, 0);

    // turn on kerning on the loaded font
    TTF_SetFontKerning(font, 1);

    // get the loaded font's max height
    SDL_Log("The font max height is: %d\n", TTF_FontHeight(font));

    // get the loaded font's max ascent
    /**
     * Get the maximum pixel ascent of all glyphs of the loaded font.
     * This can also be interpreted as the distance from the top of the font to the baseline.
     * It could be used when drawing an individual glyph relative to a top point, by combining it
     * with the glyph's maxy metric to resolve the top of the rectangle used when blitting the glyph on the screen.
     * 
     * rect.y = top + TTF_FontAscent(font) - glyph_metric.maxy;
     **/
    SDL_Log("The font ascent is: %d\n", TTF_FontAscent(font));

    // get the loaded font's max descent
    /**
     * Get the maximum pixel descent of all glyphs of the loaded font.
     * This can also be interpreted as the distance from the baseline to the bottom of the font.
     * It could be used when drawing an individual glyph relative to a bottom point, by combining it
     * with the glyph's maxy metric to resolve the top of the rectangle used when blitting the glyph on the screen.
     *
     * rect.y = bottom - TTF_FontDescent(font) - glyph_metric.maxy;
     **/
    SDL_Log("The font descent is: %d\n", TTF_FontDescent(font));

    // get the loaded font's line skip height
    /**
     * Get the recommended pixel height of a rendered line of text of the loaded font.
     * This is usually larger than the TTF_FontHeight of the font.
     **/
    SDL_Log("The font line skip is: %d\n", TTF_FontLineSkip(font));

    // get the loaded font's number of faces
    SDL_Log("The number of faces in the font is: %ld\n", TTF_FontFaces(font));

    // get the loaded font's face fixed status
    if(TTF_FontFaceIsFixedWidth(font))
        SDL_Log("The font is fixed width.\n");
    else
        SDL_Log("The font is not fixed width.\n");

    {
    // get the loaded font's face name
    char *familyname=TTF_FontFaceFamilyName(font);
    if(familyname)
        SDL_Log("The family name of the face in the font is: %s\n", familyname);
    }

    {
    // get the loaded font's face style name
    char *stylename=TTF_FontFaceStyleName(font);
    if(stylename)
        SDL_Log("The name of the face in the font is: %s\n", stylename);
    }

    {
    // check for a glyph for 'g' in the loaded font
    int index=TTF_GlyphIsProvided(font,'g');
    if(!index)
        SDL_Log("There is no 'g' in the loaded font!\n");
    }

    {
    // get the glyph metric for the letter 'g' in a loaded font
    int minx,maxx,miny,maxy,advance;
    if(TTF_GlyphMetrics(font,'g',&minx,&maxx,&miny,&maxy,&advance)==-1)
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_GlyphMetrics generated an error at line %d of file %s Error:%s\n",__LINE__,__FILE__,TTF_GetError());
    else
    {
        SDL_Log("minx    : %d\n",minx);
        SDL_Log("maxx    : %d\n",maxx);
        SDL_Log("miny    : %d\n",miny);
        SDL_Log("maxy    : %d\n",maxy);
        SDL_Log("advance : %d\n",advance);
    }
    }   

    {
    // get the width and height of a string as it would be rendered in a loaded font
    int w,h;
    if(TTF_SizeText(font,"Hello World!",&w,&h))
    {
        // perhaps print the current TTF_GetError(), the string can't be rendered...
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_SizeText generated an error at line %d of file %s Error:%s\n",__LINE__,__FILE__,TTF_GetError());
    } else
    {
        SDL_Log("width=%d height=%d\n",w,h);
    }
    }

    {
    // get the width and height of a string as it would be rendered in a loaded font
    int w,h;
    if(TTF_SizeUTF8(font,"Hello World!",&w,&h))
    {
        // perhaps print the current TTF_GetError(), the string can't be rendered...
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_SizeUTF8 generated an error at line %d of file %s Error:%s\n",__LINE__,__FILE__,TTF_GetError());
    } else
    {
        SDL_Log("width=%d height=%d\n",w,h);
    }
    }

    {
    // get the width and height of a string as it would be rendered in a loaded font
    int w,h;
    Uint16 text[]={'H','e','l','l','o',' ',
                   'W','o','r','l','d','!',0};
    if(TTF_SizeUNICODE(font,text,&w,&h))
    {
        // perhaps print the current TTF_GetError(), the string can't be rendered...
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_SizeUNICODE generated an error at line %d of file %s Error:%s\n",__LINE__,__FILE__,TTF_GetError());
    } else
    {
        SDL_Log("width=%d height=%d\n",w,h);
    }
    }


    while (running)
    {
        // SDL_Log("---------");

        // Calc past spent time
        //---

        double current_time = getTimeInSec();
        double frame_duration = current_time - old_time;
        double real_fd = frame_duration;
        double clamped_frame_duration = frame_duration;
        // SDL_Log("frame_duration %fms", frame_duration*1000);
        // SDL_Log("instant freq %fHz", 1.0/frame_duration);
        if (frame_duration > MAX_TIME)
        {
            clamped_frame_duration = MAX_TIME;
        } else
        if (frame_duration < MIN_TIME)
        {
            double ms_to_wait = (MIN_TIME - frame_duration) * 1000.0;
            // SDL_Log("sleep for %fms",ms_to_wait);

            // minimum guaranteed sleep time is 9/10ms, SDL knows this but with it we sleep ~0.1ms more (on the machine of the author)
            if (ms_to_wait>=10)
            {
                // sleep for >=10ms
                SDL_Delay(static_cast<int>(ms_to_wait));

                // busy waiting for <1ms
                double start=getTimeInSec(),i;
                int it=0;
                ms_to_wait = std::modf(ms_to_wait,&i);
                // SDL_Log("b.w. %f",ms_to_wait);
                while ((getTimeInSec()-start)*1000.0 < ms_to_wait)
                {
                    // it++;
                };
                // SDL_Log("b.w. for %d iterations",it);
            } else
            {
                //SDL_Delay(ms_to_wait); // try youtself uncommenting whis line and commenting the b.w.
                // busy waiting for max 10ms
                while ((getTimeInSec()-current_time)*1000.0 < ms_to_wait){};
            }
            current_time = getTimeInSec();
            real_fd = current_time - old_time;
            // SDL_Log("real_fd %fms", real_fd*1000);
            // SDL_Log("real instant freq %fHz", 1.0/real_fd);
            clamped_frame_duration = MIN_TIME;
        }

        old_time = current_time;
        time_accumulator += clamped_frame_duration;
        // SDL_Log("clamped %fms", clamped_frame_duration*1000);
        // SDL_Log("time_accumulator %fms", time_accumulator*1000);

        n_steps = static_cast<int>(time_accumulator/FIXED_TIMESTEP);
        // SDL_Log("n_steps %d", n_steps);


        // Event management
        //---

        // look for a close event
        while (SDL_PollEvent(&event))
        {
            switch ( event.type )
            {
            case SDL_WINDOWEVENT:
                switch (event.window.event)
                {
                case SDL_WINDOWEVENT_CLOSE:
                    SDL_Log("Window %d closed", event.window.windowID);
                    running=false;
                    break;
                default:
                    break;
                }
                break;
            case SDL_EventType::SDL_KEYUP:
                if (event.key.keysym.sym==SDLK_r)
                {
                    AtomicFluid::render_white = false;
                    AtomicFluid::render_excess = false;
                    AtomicFluid::render_alpha = !AtomicFluid::render_alpha;
                } else
                if (event.key.keysym.sym==SDLK_w)
                {
                    AtomicFluid::render_alpha = AtomicFluid::render_white;
                    AtomicFluid::render_excess = false;
                    AtomicFluid::render_white = !AtomicFluid::render_white;
                } else
                if (event.key.keysym.sym==SDLK_e)
                {
                    AtomicFluid::render_alpha = false;
                    AtomicFluid::render_white = AtomicFluid::render_excess;
                    AtomicFluid::render_excess = !AtomicFluid::render_excess;
                } else
                if (event.key.keysym.sym==SDLK_t)
                {
                    AtomicFluid::display_quantities = !AtomicFluid::display_quantities;
                } else
                if (event.key.keysym.sym==SDLK_g)
                {
                    AtomicFluid::render_grid = !AtomicFluid::render_grid;
                }
                break;

            case SDL_MOUSEMOTION:
                // event->motion.windowID, event->motion.state, event->motion.which, event->motion.x, event->motion.y, event->motion.xrel, event->motion.yrel

                if (event.motion.state & SDL_BUTTON_LMASK || insertion_locked)
                {
                    int s_gx,s_gy,e_gx,e_gy, x_step,y_step, steps;
                    AtomicFluid::mouseInGrid(event.button.x-event.motion.xrel,event.button.y-event.motion.yrel,s_gx,s_gy,grid_offset_x,grid_offset_y);
                    AtomicFluid::mouseInGrid(event.button.x,event.button.y,e_gx,e_gy,grid_offset_x,grid_offset_y);
                        if (s_gx>=0 && s_gy>=0 && s_gx<gside && s_gy<gside)
                        {
                            grid[currID][s_gy*gside+s_gx] = 
                            grid[newID][s_gy*gside+s_gx] = atoms_palette[selected_atom_palette];
                        }
                        if (e_gx>=0 && e_gy>=0 && e_gx<gside && e_gy<gside)
                        {
                            grid[currID][e_gy*gside+e_gx] = 
                            grid[newID][e_gy*gside+e_gx] = atoms_palette[selected_atom_palette];
                        }
                    if (true && (e_gx-s_gx!=0 || e_gy-s_gy!=0))
                        if (std::abs(e_gx-s_gx) >= std::abs(e_gy-s_gy))
                        {
                            steps = std::abs(e_gy-s_gy);
                            y_step = 1 * SIGN(e_gy-s_gy);
                            x_step = (e_gx-s_gx + SIGN(e_gx-s_gx)*(steps<2?1:(steps/2))) / (steps==0?1:steps);
                            int x=s_gx, y=s_gy;
                            for (int step=0; step!=steps; step++)
                            {
                                for (int s=0; s<=std::abs(x_step) && x!=e_gx+SIGN(x_step); s++, x+=SIGN(x_step))
                                {
                                    if (y>=0 && x>=0 && y<gside && x<gside)
                                    {
                                        grid[currID][y*gside+x] = 
                                        grid[newID][y*gside+x] = atoms_palette[selected_atom_palette];
                                    }
                                }
                                x-=SIGN(x_step);
                                y+=y_step;
                            }
                            while (x!=e_gx+SIGN(x_step))
                            {
                                if (y>=0 && x>=0 && y<gside && x<gside)
                                {
                                    grid[currID][y*gside+x] = 
                                    grid[newID][y*gside+x] = atoms_palette[selected_atom_palette];
                                }
                                x+=SIGN(x_step);
                            }
                        } else
                        {
                            steps = std::abs(e_gx-s_gx)+1;
                            x_step = 1 * SIGN(e_gx-s_gx);
                            y_step = (e_gy-s_gy + SIGN(e_gy-s_gy)*(steps-1)) / (steps==0?1:steps);
                            int x=s_gx, y=s_gy;
                            for (int step=0; step!=steps; step++)
                            {
                                for (int s=0; s<=std::abs(y_step) && y!=e_gy+SIGN(y_step); s++, y+=SIGN(y_step))
                                {
                                    if (y>=0 && x>=0 && y<gside && x<gside)
                                    {
                                        grid[currID][y*gside+x] = 
                                        grid[newID][y*gside+x] = atoms_palette[selected_atom_palette];
                                    }
                                }
                                y-=SIGN(y_step);
                                x+=x_step;
                            }
                            while (y!=e_gy+SIGN(y_step) && y<gside && x<gside)
                            {
                                if (y>=0 && x>=0 && y<gside && x<gside)
                                {
                                    grid[currID][y*gside+x] = 
                                    grid[newID][y*gside+x] = atoms_palette[selected_atom_palette];
                                }
                                y+=SIGN(y_step);
                            }
                        }
                }
                break;
            case SDL_MOUSEBUTTONDOWN:
                // event->button.windowID, event->button.which,  event->button.button, event->button.state, event->button.x, event->button.y

                switch(event.button.button)
                {
                    case SDL_BUTTON_LEFT:
                    {
                        if (event.button.clicks==2)
                        {
                            insertion_locked = !insertion_locked;
                        }
                        mouse.left = true;

                        int x,y;
                        AtomicFluid::mouseInGrid(event.button.x,event.button.y,x,y,grid_offset_x,grid_offset_y);
                        if (y>=0 && x>=0 && y<gside && x<gside)
                        {
                            grid[currID][y*gside+x] = 
                            grid[newID][y*gside+x] = atoms_palette[selected_atom_palette];
                        }
                        break;
                    }
                    case SDL_BUTTON_RIGHT:
                    {
                        mouse.right = true;
                        break;
                    }
                }
                break;

            case SDL_MOUSEBUTTONUP:
                switch(event.button.button)
                {
                    case SDL_BUTTON_LEFT:
                    {
                        mouse.left = false;
                        break;
                    }
                    case SDL_BUTTON_RIGHT:
                    {
                        mouse.right = false;
                        break;
                    }
                }
                break;

            case SDL_MOUSEWHEEL:
            {
                int np = sizeof(atoms_palette)/sizeof(AtomicFluid::Atom);
                selected_atom_palette = ((selected_atom_palette + event.button.x)%np+np)%np;
            }
                break;
            default:
                break;
            }
        }


        // partial editor command [if mouse is not moved but pressed]
        if (mouse.left || insertion_locked)
        {
            int x, y;
            AtomicFluid::mouseInGrid(mouse.x,mouse.y,x,y,grid_offset_x,grid_offset_y);
            if (y>=0 && x>=0 && y<gside && x<gside)
            {
                grid[currID][y*gside+x] = 
                grid[newID][y*gside+x] = atoms_palette[selected_atom_palette];
            }
        }


        // Fixed time-step update
        //---

        if (n_steps > 0)
        {
            time_accumulator -= n_steps*FIXED_TIMESTEP;
            // SDL_Log("over %fms", time_accumulator*1000);

            for (int s=0; s!=n_steps; s++)
            {
                /**
                 * for each element
                 * previous_state = current_state
                 * fixedTSUpdate(game_time, FIXED_TIMESTEP)
                 **/

                AtomicFluid::fixedTSGridUpdate(grid[currID], grid[newID], gside, gside);
                std::swap(currID, newID);
                for (int i=0; i!=gside; i++)
                    for (int j=0; j!=gside; j++)
                        grid[newID][i*gside+j] = grid[currID][i*gside+j];

                game_time += FIXED_TIMESTEP;
            }
        }
        // SDL_Log("time_accumulator %fms", time_accumulator*1000);

        alpha = time_accumulator / FIXED_TIMESTEP;


        // Time dependant update
        //---

        /**
         * for each element
         * update(game_time, n_steps*FIXED_TIMESTEP)
         **/


        // Smooth
        //---

        /**
         * for each element
         * current_state = current_state * alpha + previous_state * (1.0 - alpha)
         */


        // Render
        //---

        // set the color to draw with
        SDL_SetRenderDrawColor(
                            renderer,    // SDL_Renderer* renderer: the renderer to affect
                            AtomicFluid::clear_color.r,          // Uint8 r: Red
                            AtomicFluid::clear_color.g,          // Uint8 g: Green
                            AtomicFluid::clear_color.b,          // Uint8 b: Blue
                            AtomicFluid::clear_color.a);        // Uint8 a: Alpha
        // Clear the entire screen to our selected color.
        SDL_RenderClear(renderer);

        // <Render all the elements here>

        AtomicFluid::renderGrid(grid[currID], gside, gside, grid_offset_x, grid_offset_y);

        /**
         * These functions render text using a TTF_Font.
         * There are three modes of rendering:
         * 
         * Solid
         *     Quick and Dirty
         *     Create an 8-bit palettized surface and render the given text at fast quality with the given font and color.
         *     The pixel value of 0 is the colorkey, giving a transparent background when blitted. Pixel and colormap value 1
         *     is set to the text foreground color. This allows you to change the color without having to render the text again.
         *     Palette index 0 is of course not drawn when blitted to another surface, since it is the colorkey, and thus
         *     transparent, though its actual color is 245 minus each of the RGB components of the foreground color.
         *     This is the fastest rendering speed of all the rendering modes. This results in no box around the text, but the
         *     text is not as smooth. The resulting surface should blit faster than the Blended one. Use this mode for FPS and
         *     other fast changing updating text displays. 
         * Shaded
         *     Slow and Nice, but with a Solid Box
         *     Create an 8-bit palettized surface and render the given text at high quality with the given font and colors.
         *     The 0 pixel value is background, while other pixels have varying degrees of the foreground color from the
         *     background color. This results in a box of the background color around the text in the foreground color.
         *     The text is antialiased. This will render slower than Solid, but in about the same time as Blended mode.
         *     The resulting surface should blit as fast as Solid, once it is made. Use this when you need nice text, and
         *     can live with a box. 
         * Blended
         *     Slow Slow Slow, but Ultra Nice over another image
         *     Create a 32-bit ARGB surface and render the given text at high quality, using alpha blending to dither the
         *     font with the given color. This results in a surface with alpha transparency, so you don't have a solid colored
         *     box around the text. The text is antialiased. This will render slower than Solid, but in about the same time as
         *     Shaded mode. The resulting surface will blit slower than if you had used Solid or Shaded. Use this when you want
         *     high quality, and the text isn't changing too fast. 
         * 
         *     Solid
         *     3.4.1 TTF_RenderText_Solid       Draw LATIN1 text in solid mode
         *     3.4.2 TTF_RenderUTF8_Solid       Draw UTF8 text in solid mode
         *     3.4.3 TTF_RenderUNICODE_Solid    Draw UNICODE text in solid mode
         *     3.4.4 TTF_RenderGlyph_Solid      Draw a UNICODE glyph in solid mode
         *     Shaded
         *     3.4.5 TTF_RenderText_Shaded      Draw LATIN1 text in shaded mode
         *     3.4.6 TTF_RenderUTF8_Shaded      Draw UTF8 text in shaded mode
         *     3.4.7 TTF_RenderUNICODE_Shaded   Draw UNICODE text in shaded mode
         *     3.4.8 TTF_RenderGlyph_Shaded     Draw a UNICODE glyph in shaded mode
         *     Blended
         *     3.4.9 TTF_RenderText_Blended     Draw LATIN1 text in blended mode
         *     3.4.10 TTF_RenderUTF8_Blended    Draw UTF8 text in blended mode
         *     3.4.11 TTF_RenderUNICODE_Blended Draw UNICODE text in blended mode
         *     3.4.12 TTF_RenderGlyph_Blended   Draw a UNICODE glyph in blended mode
         **/
#if 1
        // Solid
        {
            // Render some text in solid black to a new surface
            // then create a texture from it
            // then free the text surface
            SDL_Color color={255,255,255};
            SDL_Surface *text_surface;
            SDL_Texture *text_tex;
            char text[1024];
            // SDL_Log("n_steps %d", n_steps);
            // SDL_Log("clamped %fms", clamped_frame_duration*1000);
            // SDL_Log("real_fd %fms", real_fd*1000);
            // SDL_Log("real instant freq %fHz", 1.0/real_fd);
            // SDL_Log("frame_duration %fms", frame_duration*1000);
            // SDL_Log("instant freq %fHz", 1.0/frame_duration);
            sprintf(text,"frame duration: %7.2fms instant freq %7.2fHz |#| real_fd %7.2fms real instant freq %7.2fHz          atom type:%s",
                          frame_duration*1000, 1.0/frame_duration, real_fd*1000,    1.0/real_fd, atoms_palette_names[selected_atom_palette]);
            if(!(text_surface=TTF_RenderText_Solid(font, text, color)))
            {
                // handle error here, perhaps print TTF_GetError at least
                SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_RenderText_Solid generated an error at line %d of file %s Error:%s\n",__LINE__,__FILE__,TTF_GetError());
            } else
            {

                SDL_Rect tex_size = {0,0,0,0};
                SDL_Rect tex_dest;

                tex_size.w = text_surface->w;
                tex_size.h = text_surface->h;
                tex_dest.w = tex_size.w;
                tex_dest.h = tex_size.h;

                {
                    // int ww, wh;
                    // SDL_GetWindowSize(  window, // SDL_Window* window: the window to query
                    //                     &ww,    // int*        w: gets the width of the window
                    //                     &wh);   // int*        h: gets the height of the window

                    tex_dest.x = 10;//(ww - tex_dest.w)/2;
                    tex_dest.y = 10;//(wh - tex_dest.h)/2-50;
                }


                text_tex = SDL_CreateTextureFromSurface(renderer, text_surface);
                if (!text_tex)
                {
                    const char *error = SDL_GetError();
                    if (*error != '\0')
                    {
                        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not rendercopy. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
                        SDL_ClearError();
                    }
                }
                SDL_FreeSurface(text_surface);

                ret =
                SDL_RenderCopy( renderer,    // SDL_Renderer* renderer: the renderer to affect
                                text_tex,    // SDL_Texture* texture: the source texture
                                &tex_size,   // const SDL_Rect* srcrect: the source SDL_Rect structure or NULL for the entire texture  
                                &tex_dest);  // const SDL_Rect* dstrect: the destination SDL_Rect structure or NULL for the entire rendering target. The texture will be stretched to fill the given rectangle.
                if (ret != 0)
                {
                    const char *error = SDL_GetError();
                    if (*error != '\0')
                    {
                        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not rendercopy. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
                        SDL_ClearError();
                    }
                }

                // Destroy the texture
                SDL_DestroyTexture(text_tex);
            }
        }
#endif
#if 0
        // Shaded
        {
            // Render some text in solid black to a new surface
            // then create a texture from it
            // then free the text surface
            SDL_Color color={0,0,0}, bgcolor={255,255,255};
            SDL_Surface *text_surface;
            SDL_Texture *text_tex;
            char text[1024];
            sprintf(text,"Hello World! %d",SDL_GetTicks());
            if(!(text_surface=TTF_RenderText_Shaded(font, text, color, bgcolor)))
            {
                // handle error here, perhaps print TTF_GetError at least
                SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_RenderText_Solid generated an error at line %d of file %s Error:%s\n",__LINE__,__FILE__,TTF_GetError());
            } else
            {

                SDL_Rect tex_size = {0,0,0,0};
                SDL_Rect tex_dest;

                tex_size.w = text_surface->w;
                tex_size.h = text_surface->h;
                tex_dest.w = tex_size.w;
                tex_dest.h = tex_size.h;

                {
                    int ww, wh;
                    SDL_GetWindowSize(  window, // SDL_Window* window: the window to query
                                        &ww,    // int*        w: gets the width of the window
                                        &wh);   // int*        h: gets the height of the window

                    tex_dest.x = (ww - tex_dest.w)/2;
                    tex_dest.y = (wh - tex_dest.h)/2;
                }


                text_tex = SDL_CreateTextureFromSurface(renderer, text_surface);
                if (!text_tex)
                {
                    const char *error = SDL_GetError();
                    if (*error != '\0')
                    {
                        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not rendercopy. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
                        SDL_ClearError();
                    }
                }
                SDL_FreeSurface(text_surface);

                ret =
                SDL_RenderCopy( renderer,    // SDL_Renderer* renderer: the renderer to affect
                                text_tex,    // SDL_Texture* texture: the source texture
                                &tex_size,   // const SDL_Rect* srcrect: the source SDL_Rect structure or NULL for the entire texture  
                                &tex_dest);  // const SDL_Rect* dstrect: the destination SDL_Rect structure or NULL for the entire rendering target. The texture will be stretched to fill the given rectangle.
                if (ret != 0)
                {
                    const char *error = SDL_GetError();
                    if (*error != '\0')
                    {
                        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not rendercopy. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
                        SDL_ClearError();
                    }
                }

                // Destroy the texture
                SDL_DestroyTexture(text_tex);
            }
        }

        // Blended
        {
            // Render some text in solid black to a new surface
            // then create a texture from it
            // then free the text surface
            SDL_Color color={0,0,0};
            SDL_Surface *text_surface;
            SDL_Texture *text_tex;
            char text[1024];
            sprintf(text,"Hello World! %d",SDL_GetTicks());
            if(!(text_surface=TTF_RenderText_Blended(font, text, color)))
            {
                // handle error here, perhaps print TTF_GetError at least
                SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_RenderText_Solid generated an error at line %d of file %s Error:%s\n",__LINE__,__FILE__,TTF_GetError());
            } else
            {

                SDL_Rect tex_size = {0,0,0,0};
                SDL_Rect tex_dest;

                tex_size.w = text_surface->w;
                tex_size.h = text_surface->h;
                tex_dest.w = tex_size.w;
                tex_dest.h = tex_size.h;

                {
                    int ww, wh;
                    SDL_GetWindowSize(  window, // SDL_Window* window: the window to query
                                        &ww,    // int*        w: gets the width of the window
                                        &wh);   // int*        h: gets the height of the window

                    tex_dest.x = (ww - tex_dest.w)/2;
                    tex_dest.y = (wh - tex_dest.h)/2+50;
                }


                text_tex = SDL_CreateTextureFromSurface(renderer, text_surface);
                if (!text_tex)
                {
                    const char *error = SDL_GetError();
                    if (*error != '\0')
                    {
                        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not rendercopy. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
                        SDL_ClearError();
                    }
                }
                SDL_FreeSurface(text_surface);

                ret =
                SDL_RenderCopy( renderer,    // SDL_Renderer* renderer: the renderer to affect
                                text_tex,    // SDL_Texture* texture: the source texture
                                &tex_size,   // const SDL_Rect* srcrect: the source SDL_Rect structure or NULL for the entire texture  
                                &tex_dest);  // const SDL_Rect* dstrect: the destination SDL_Rect structure or NULL for the entire rendering target. The texture will be stretched to fill the given rectangle.
                if (ret != 0)
                {
                    const char *error = SDL_GetError();
                    if (*error != '\0')
                    {
                        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not rendercopy. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
                        SDL_ClearError();
                    }
                }

                // Destroy the texture
                SDL_DestroyTexture(text_tex);
            }
        }
#endif
        if (ret != 0)
        {
            const char *error = SDL_GetError();
            if (*error != '\0')
            {
                SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not rendercopy. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
                SDL_ClearError();
            }
        }
        
        // update the screen with the performed rendering
        SDL_RenderPresent(renderer);
    }


    // Exit
    //---

    // Destroy renderer
    SDL_DestroyRenderer(renderer);

    // Close and destroy the window
    SDL_DestroyWindow(window);

    // Quit SDL
    SDL_Quit();

    // Close the font
    TTF_CloseFont(font);

    // Quit SDL_ttf
    if (TTF_WasInit())
    {
        TTF_Quit();
    }
    return 0;
}
