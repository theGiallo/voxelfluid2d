################################################################################
# This is a very basic makefile. Later then we will improve it.
################################################################################

ARCH := $(shell getconf LONG_BIT)

LIB_SDL2_64=-L"libraries/SDL2/linux/lib64" -Wl,-rpath=./libraries/SDL2/linux/lib64 -lSDL2
LIB_SDL2_32=-L"libraries/SDL2/linux/lib32" -Wl,-rpath=./libraries/SDL2/linux/lib32 -lSDL2
LIB_SDL2=$(LIB_SDL2_$(ARCH))
I_SDL2=-I"libraries/SDL2/linux/include"
LIB_SDL2_TTF_64=-L"libraries/SDL2_ttf/linux/lib64" -Wl,-rpath=./libraries/SDL2_ttf/linux/lib64 -lSDL2_ttf
LIB_SDL2_TTF_32=-L"libraries/SDL2_ttf/linux/lib32" -Wl,-rpath=./libraries/SDL2_ttf/linux/lib32 -lSDL2_ttf
LIB_SDL2_TTF=$(LIB_SDL2_TTF_$(ARCH))
I_SDL2_TTF=-I"libraries/SDL2_ttf/linux/include" -iquote"libraries/SDL2/linux/include/SDL2"
LIBS=$(LIB_SDL2)
INCLUDES=$(I_SDL2)
CFLAGS= -std=c++11
OPT_RELEASE= -O3
OPT_DEBUG= -O0 -g3
DEBUG_FLAGS= -DDEBUG $(OPT_DEBUG)
RELEASE_FLAGS= $(OPT_RELEASE)

all: release debug
release: voxelwatertest00 voxelwatertest01 voxelwatertest02
debug: voxelwatertest00_d voxelwatertest01_d voxelwatertest02_d

###############
### release ###

voxelwatertest00: voxelwatertest00.cpp Makefile
	g++ voxelwatertest00.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(RELEASE_FLAGS) -o "voxelwatertest00" $(I_SDL2_TTF) $(LIB_SDL2_TTF)
voxelwatertest01: voxelwatertest01.cpp Makefile
	g++ voxelwatertest01.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(RELEASE_FLAGS) -o "voxelwatertest01" $(I_SDL2_TTF) $(LIB_SDL2_TTF)
voxelwatertest02: voxelwatertest02.cpp Makefile
	g++ voxelwatertest02.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(RELEASE_FLAGS) -o "voxelwatertest02" $(I_SDL2_TTF) $(LIB_SDL2_TTF)

#############
### debug ###

voxelwatertest00_d: voxelwatertest00.cpp Makefile
	g++ voxelwatertest00.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(DEBUG_FLAGS) -o "voxelwatertest00_d" $(I_SDL2_TTF) $(LIB_SDL2_TTF)
voxelwatertest01_d: voxelwatertest01.cpp Makefile
	g++ voxelwatertest01.cpp $(CFLAGS) $(INCLUDES) $(LIBS) $(DEBUG_FLAGS) -o "voxelwatertest01_d" $(I_SDL2_TTF) $(LIB_SDL2_TTF)
voxelwatertest02_d: voxelwatertest02.cpp Makefile
	g++ $(CFLAGS) $(INCLUDES) $(LIBS) $(DEBUG_FLAGS) voxelwatertest02.cpp -o "voxelwatertest02_d" $(I_SDL2_TTF) $(LIB_SDL2_TTF)

clean:
	rm voxelwatertest00 voxelwatertest00_d voxelwatertest01 voxelwatertest01_d voxelwatertest02 voxelwatertest02_d

.PHONY: all clean